import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EmployyeComponent } from './employye/employye.component';
import { Com2Component } from './com2/com2.component';
import { ComtwoComponent } from './comtwo/comtwo.component';
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from './app.routing';
import { ComthreeComponent } from './comthree/comthree.component';
import { registerationComponent} from './registeration/registeration.component';
import {HttpClientModule} from "@angular/common/http";
import { ConfirmComponent } from './confirm/confirm.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployyeComponent,
    Com2Component,
    ComtwoComponent,
    ComthreeComponent,
    registerationComponent,
    ConfirmComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
