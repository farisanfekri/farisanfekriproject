import { Component, OnInit } from '@angular/core';
import {RestClientService} from "../rest-client.service";
import {Employee} from './employye';

@Component({
  selector: 'app-employye',
  templateUrl: './employye.component.html',
  styleUrls: ['./employye.component.css']
})
export class EmployyeComponent implements OnInit {

  constructor(private restClient: RestClientService) { }

  employeeList = [];
  employee: Employee;

  ngOnInit() {

    this.employee = {name: '', salary: null, age: null, id: null};

this.restClient.getEmployees().subscribe(res => {
    console.log('employye list', res);
    this.employeeList = res.data;
  });

  }

}
