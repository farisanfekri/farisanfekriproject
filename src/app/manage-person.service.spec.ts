import { TestBed } from '@angular/core/testing';

import { ManagePersonService } from './manage-person.service';

describe('ManagePersonService', () => {
  let service: ManagePersonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManagePersonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
