import { Component, OnInit } from '@angular/core';
import {Person} from './person';
import {Router} from '@angular/router';
import {ManagePersonService} from '../manage-person.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class registerationComponent implements OnInit {

  constructor(private router: Router,
              private personManager: ManagePersonService) { }

  person: Person = {firstName: '', lastName: '', age: null, description: '', mobileNumber: '', birthDate: {}, education: {}};
  message = '';

  educations = [
    {name: 'کاردانی', code: '1'},
    {name: 'لیسانس', code: '2'},
    {name: 'فوق لیسانس', code: '3'},
    {name: 'سایر', code: '4'}
  ];

  ngOnInit() {
    // this.myDate = moment('1395-11-22', 'jYYYY,jMM,jDD');
  }

  register() {
    // this.message = 'You have registered successfully';
    // sessionStorage.setItem('person', JSON.stringify(this.person));
    this.personManager.savePerson(this.person);
    this.router.navigateByUrl('confirm');
  }

}
