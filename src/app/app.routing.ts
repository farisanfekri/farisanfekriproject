import {RouterModule, Routes} from "@angular/router";
import {ComtwoComponent} from "./comtwo/comtwo.component";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {ComthreeComponent} from "./comthree/comthree.component";
import {EmployyeComponent} from "./employye/employye.component";
import {ConfirmComponent} from "./confirm/confirm.component";
import {RegisterationComponent} from "./registeration/registeration.component";

const routes: Routes=[
  {path:'comtwo', component:ComtwoComponent},
  { path: 'comthree', component: ComthreeComponent},
  { path: 'employye', component: EmployyeComponent},
  { path: 'confirm', component: ConfirmComponent},
  { path: 'register', component: RegisterationComponent}
]
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule {}


